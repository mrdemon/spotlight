//
//  ViewController.m
//  KivaJSONDemo
//
//  Created by Ray Wenderlich on 10/3/11.
//  Copyright 2011 Razeware LLC. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) //1
#define kLatestKivaLoansURL [NSURL URLWithString: @"http://api.kivaws.org/v1/loans/search.json?status=fundraising"] //2

#import "ViewController.h"

@interface NSDictionary(JSONCategories)
+(NSDictionary*)dictionaryWithContentsOfJSONURLString:(NSString*)urlAddress;
-(NSData*)toJSON;
@end

@implementation NSDictionary(JSONCategories)
+(NSDictionary*)dictionaryWithContentsOfJSONURLString:(NSString*)urlAddress
{
    NSData* data = [NSData dataWithContentsOfURL: [NSURL URLWithString: urlAddress] ];
    __autoreleasing NSError* error = nil;
    id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    if (error != nil) return nil;
    return result;
}

-(NSData*)toJSON
{
    NSError* error = nil;
    id result = [NSJSONSerialization dataWithJSONObject:self options:kNilOptions error:&error];
    if (error != nil) return nil;
    return result;
}
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initLimitlessLED ];
  
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.png"]]];
//    [self.view setBackgroundColor:[UIColor colorWithWhite:0.9 alpha:1]];
    dispatch_async(kBgQueue, ^{
        NSData* data = [NSData dataWithContentsOfURL: kLatestKivaLoansURL];
        [self performSelectorOnMainThread:@selector(fetchedData:) withObject:data waitUntilDone:YES];
    });
    pomodoroTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(pomodoroTimerCallback:) userInfo:nil repeats:YES];
    
    UIImageView* timeBG = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"time.png"]];
    timeBG.frame = CGRectMake(28.5, 97.5, 264, 93);
    [self.view addSubview:timeBG];
    
    curTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(28.5, 97.5, 204, 93)];
    curTimeLabel.backgroundColor = [UIColor clearColor];
    curTimeLabel.textAlignment = NSTextAlignmentCenter;
    curTimeLabel.font = [UIFont boldSystemFontOfSize:62];
    NSDate* curDate = [NSDate date];
    dateFormater = [[NSDateFormatter alloc] init];
    [dateFormater setDateFormat:@"HH mm"];
    curTimeLabel.text = [dateFormater stringFromDate:curDate];
    [self.view addSubview:curTimeLabel];
    
    curSecsLabel = [[UILabel alloc] initWithFrame:CGRectMake(220, 97.5, 55, 93)];
    curSecsLabel.backgroundColor = [UIColor clearColor];
    curSecsLabel.textAlignment = NSTextAlignmentCenter;
    curSecsLabel.font = [UIFont boldSystemFontOfSize:42];
    SecsFormater = [[NSDateFormatter alloc] init];
    [SecsFormater setDateFormat:@"ss"];
    curSecsLabel.text = [SecsFormater stringFromDate:curDate];
    [self.view addSubview:curSecsLabel];
    
    UIImageView* gleaf = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"leaf1.png"]];
    gleaf.frame = CGRectMake(17.5, 12.5, 115, 62);
    [self.view addSubview:gleaf];
    gLeafLabel = [[UILabel alloc] initWithFrame:CGRectMake(95, 27, 50, 42)];
    gLeafLabel.backgroundColor = [UIColor clearColor];
    gLeafLabel.font = [UIFont boldSystemFontOfSize:36];
    gLeafLabel.text = [NSString stringWithFormat:@"%d", gLeafCount];
    gLeafLabel.textColor = [UIColor whiteColor];
    [self.view addSubview:gLeafLabel];
    
    UIImageView* bleaf = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"leaf2.png"]];
    bleaf.frame = CGRectMake(180, 12.5, 115, 62);
    [self.view addSubview:bleaf];
    bLeafLabel = [[UILabel alloc] initWithFrame:CGRectMake(258, 27, 50, 42)];
    bLeafLabel.backgroundColor = [UIColor clearColor];
    bLeafLabel.font = [UIFont boldSystemFontOfSize:36];
    bLeafLabel.text = [NSString stringWithFormat:@"%d", bLeafCount];
    bLeafLabel.textColor = [UIColor whiteColor];
    [self.view addSubview:bLeafLabel];
    
    UIImageView* circleBG = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"controller.png"]];
    circleBG.frame = CGRectMake(33.5, 206.5, 250.5, 250.5);
    [self.view addSubview:circleBG];
    circleView = [[CircleUpdateView alloc] initWithFrame:CGRectMake(33.5, 206.5, 250.5, 250.5)];
    circleView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:circleView];
    
    actionBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [actionBtn setImage:[UIImage imageNamed:@"trigger.png"] forState:UIControlStateNormal];
//    [actionBtn setTitle:@"Start Work" forState:UIControlStateNormal];
    [actionBtn addTarget:self action:@selector(onAction:) forControlEvents:UIControlEventTouchUpInside];
    actionBtn.frame = CGRectMake(70.25, 243.75, 177, 176);
    actionBtn.enabled = YES;
    [self.view addSubview:actionBtn];
    
    UIButton* infoBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [infoBtn setImage:[UIImage imageNamed:@"info.png"] forState:UIControlStateNormal];
    infoBtn.frame = CGRectMake(266.5, 437.5, 43, 35);
    [self.view addSubview:infoBtn];
    
    gLeafCount = 0;
    bLeafCount = 0;
    // Setup test data
    currentType = 0;
//    workSecs = 60*25;
//    sRestSecs = 60*5;
//    lRestSecs = 60*15;
    workSecs = 30;
    sRestSecs = 10;
    lRestSecs = 20;
    lRestInterval = 3;
    curSRestTimes = lRestInterval;
    
    //[self triggerSpotLight];
   
   
}

-(void) initLimitlessLED
{
    udpSocket = [[AsyncUdpSocket alloc] initWithDelegate:self];
	
	NSError *error = nil;
	
	if (![udpSocket bindToPort:0 error:&error])
	{
		NSLog(@"Error binding: %@", error);
		return;
	}

    // {0x39, 0x00, 0x55} - 白光
    char bytes[] ={0x20, 0x11, 0x55};

    NSData * data = [NSData dataWithBytes:bytes length:sizeof(bytes)];

    [udpSocket sendData:data toHost:@"192.168.1.100" port:50000 withTimeout:-1 tag:1];
       
}

/**
 * Called when the datagram with the given tag has been sent.
 **/
- (void)onUdpSocket:(AsyncUdpSocket *)sock didSendDataWithTag:(long)tag {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (void)pomodoroTimerCallback:(NSTimer*)timer{
    // Sync current time
    NSDate* curTime = [NSDate date];
    curTimeLabel.text = [dateFormater stringFromDate:curTime];
    curSecsLabel.text = [SecsFormater stringFromDate:curTime];
    
//    NSLog(@"old type is %d", currentType);
    if (currentType == 1) {
        // Check if it's time to taking rest
        NSDate* nextRestTime = [[NSDate alloc] initWithTimeInterval:workSecs sinceDate:startTime];
        if ([nextRestTime compare:curTime] != NSOrderedDescending) {
            // It is time to rest
            curSRestTimes--;
            if (curSRestTimes <= 0) {
                // it's time to have large rest
                curSRestTimes = lRestInterval;
                [self enterRest:YES];
            }else{
                // It's time to have small rest
                [self enterRest:NO];
            }
        }else{
            NSTimeInterval passedSecs = [nextRestTime timeIntervalSinceDate:curTime];
            int newRate = passedSecs * 100 / workSecs;
            [circleView updateFillRate:newRate];
        }
    }else if (currentType == 2 || currentType == 3) {
        // Check if it's time to working
        NSTimeInterval restSecs = (currentType == 2) ? sRestSecs : lRestSecs;
        NSDate* nextWorkTime = [[NSDate alloc] initWithTimeInterval:restSecs sinceDate:startTime];
        if ([nextWorkTime compare:curTime] != NSOrderedDescending) {
            // It is time to start work
            currentType = 0;
            [circleView updateFillRate:100];
            gLeafCount++;
            gLeafLabel.text = [NSString stringWithFormat:@"%d", gLeafCount];
        }else{
            NSTimeInterval passedSecs = [nextWorkTime timeIntervalSinceDate:curTime];
            int newRate = passedSecs * 100 / restSecs;
            if (newRate == 0) newRate = 100;
            [circleView updateFillRate:newRate];
        }
    }
//    NSLog(@"new type is %d", currentType);
}

- (IBAction)onAction:(UIButton*)sender{
    
       
    if (currentType != 1) {
        // Check if user enter work normally
        if (currentType != 0) {
            // User start in the rest time
            bLeafCount++;
            bLeafLabel.text = [NSString stringWithFormat:@"%d", bLeafCount];
        }
        // Enter work type
        [self enterWork];
    }else{
        // Check if user enter rest normally
        // Enter rest type
        [self enterRest:NO];
    }
}

- (void)enterWork{
//    [actionBtn setTitle:@"Start Rest" forState:UIControlStateNormal];
    [self changeBulb:1 hue:0];
    currentType = 1;
    startTime = [NSDate date];
    circleView.clockwise = 0;
    circleView.circleColor = [UIColor redColor];
    [circleView updateFillRate:100];
    NSLog(@"Enter Work ! ");
}

- (void)enterRest:(BOOL)isLarge{
//    [actionBtn setTitle:@"Start Work" forState:UIControlStateNormal];
    float brightness = isLarge ? 0.1 : 0.2;
    [self changeBulb:brightness hue:0];
    currentType = isLarge ? 3 : 2;
    startTime = [NSDate date];
    circleView.clockwise = 1;
    circleView.circleColor = [UIColor blueColor];
    [circleView updateFillRate:0];
    NSLog(@"Enter Rest ! type = %d", currentType);
}

- (void)triggerSpotLight{
    manager = [[CMMotionManager alloc] init];
    manager.deviceMotionUpdateInterval = 1.0/60.0;
    if (manager.isDeviceMotionAvailable) {
        [manager startDeviceMotionUpdates];
    }
    
    NSURL* url = [NSURL fileURLWithPath:@"/dev/null"];
    NSDictionary* settings = [NSDictionary dictionaryWithObjectsAndKeys:
                              [NSNumber numberWithFloat:44100.0],                   AVSampleRateKey,
                              [NSNumber numberWithInt:kAudioFormatAppleLossless],   AVFormatIDKey,
                              [NSNumber numberWithInt:1],                           AVNumberOfChannelsKey,
                              [NSNumber numberWithInt:AVAudioQualityMax],           AVEncoderAudioQualityKey ,nil];
    
    NSError* error;
    recorder = [[AVAudioRecorder alloc] initWithURL:url settings:settings error:&error];
    if (recorder) {
        [recorder prepareToRecord];
        recorder.meteringEnabled = YES;
        [recorder record];
        levelTimer = [NSTimer scheduledTimerWithTimeInterval:0.03 target:self selector:@selector(levelTimerCallback:) userInfo:nil repeats:YES];
    }else{
        NSLog([error description]);
    }
}

- (void)levelTimerCallback:(NSTimer*)timer{
    [recorder updateMeters];
    
    const double ALPHA = 0.05;
    double peakPowerForChannel = pow(10, (0.05 * [recorder peakPowerForChannel:0]));
    int oldLowPassLevel = [self getLowPassLevel:lowPassResults];
    lowPassResults = ALPHA * peakPowerForChannel + (1.0 - ALPHA) * lowPassResults;

    int lowPassLevel = [self getLowPassLevel:lowPassResults];
    
    //NSLog(@"Average input: %f Peak input: %f", [recorder averagePowerForChannel:0], [recorder peakPowerForChannel:0]);
    
    CMDeviceMotion* curDevice = manager.deviceMotion;
    CMAttitude* curAttitude = curDevice.attitude;
    float roll = curAttitude.roll;
//    float pitch = curAttitude.pitch;
//    float yaw = curAttitude.yaw;
    
    //NSLog(@"roll: %.2f, pitch: %.2f, yaw: %.2f", roll, pitch, yaw);
    int oldRollLevel = rollLevel;
    rollLevel = [self getRollLevel:roll];
    BOOL hueChange = oldRollLevel != rollLevel;
    
    if (oldLowPassLevel != lowPassLevel || hueChange) {
        //float hue = [self getHueFromRollLevel:rollLevel];
        if (lowPassLevel == 1) {
            [self onAction:actionBtn];
            //self.view.backgroundColor = [UIColor whiteColor];
            //[self changeBulb:1 hue:hue];
            //NSLog(@"Mic blow detected");
        }else if (lowPassLevel == 2){
            //self.view.backgroundColor = [UIColor lightGrayColor];
            //[self changeBulb:0.8 hue:hue];
            //NSLog(@"Level unknown 2");
        }else if (lowPassLevel == 3){
            //self.view.backgroundColor = [UIColor darkGrayColor];
            //[self changeBulb:0.6 hue:hue];
            //NSLog(@"Level unknown 3");
        }else{
            //self.view.backgroundColor = [UIColor blackColor];
            //[self changeBulb:0.4 hue:hue];
            //NSLog(@"Level unknown 4");
        }
    }
}

- (float)getHueFromRollLevel:(int)level{
    if (level == 2) {
        return 0.8;
    }else if (level == 1){
        return 0.2;
    }else{
        return 0.6;
    }
}

- (int)getRollLevel:(float)roll{
    if (roll > 0.5) {
        return 2;
    }else if (roll < -0.5){
        return 1;
    }else{
        return 0;
    }
}

- (int)getLowPassLevel:(double)lowPassResult{
    if (lowPassResults > 0.9) {
        return 1;
    }else if (lowPassResults > 0.5){
        return 2;
    }else if (lowPassResults > 0.2){
        return 3;
    }else{
        return 4;
    }
}

- (void)fetchedData:(NSData *)responseData {
    //parse out the json data
    NSError* error;
    
    //>>---------------  HTTP Request -------------------------------------------------
    
    /* Json format
     {
     "user": {
     "email": "hugo.rian.2@gmail.com",
     "password": "hugotest"
     }
     */
    
    //build an info object and convert to json
    NSDictionary* loginDetail = [NSDictionary dictionaryWithObjectsAndKeys:
                                 @"hackathon2@huantengsmart.com", @"email",
                                 @"9bah4o", @"password",
                                 nil];
    NSDictionary* loginData = [NSDictionary dictionaryWithObjectsAndKeys:
                               loginDetail, @"user",
                               nil];
    //convert object to data
    NSData* jsonDataGreen = [NSJSONSerialization dataWithJSONObject:loginData
                                                            options:NSJSONWritingPrettyPrinted
                                                              error:&error];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:@"https://huantengsmart.com/users/sign_in.json"]]; // Assumes you have created an NSURL * in "myURL"
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:jsonDataGreen];
    [request setValue:@"application/json" forHTTPHeaderField:@"content-type"];
    
    
    NSError *theError = nil;
    NSURLResponse *theResponse =[[NSURLResponse alloc]init];
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&theResponse error:&theError];
    NSLog(@"response : %@", theResponse);
    NSLog(@"data : %@", data);
    
    NSString *string = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"string: %@", string);
    
    //>>---------------  Get Token -------------------------------------------------
    
    if (data ==nil) {
        return;
    }
    NSDictionary* jsonBack = [NSJSONSerialization JSONObjectWithData:data //1
                                                             options:kNilOptions
                                                               error:&error];
    token = [jsonBack objectForKey:@"auth_token"] ;
    NSLog(@"token: %@", token);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        // UI thread
        actionBtn.enabled = YES;
        // Enter rest time by default
        [self changeBulb:0.2 hue:0];
    });
    
}

- (void)changeBulb:(float)brightness hue:(float)hue{
    //>>------------------Change bulb---------------------------------------------------------------
    dispatch_async(kBgQueue, ^{
        NSError* error;
        
        NSMutableDictionary* bublDetail = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    @"59", @"id",
                                    [NSString stringWithFormat:@"%f", brightness], @"brightness",
                                    @"true", @"turned_on",
                                    nil];
        
        if (hue != 0) {
            // Add hue value
            [bublDetail setObject:[NSString stringWithFormat:@"%f", hue] forKey:@"hue"];
        }
        
        NSArray *myArray = [[NSArray alloc] initWithObjects:bublDetail, nil];
        
        
        NSDictionary* bulbData = [NSDictionary dictionaryWithObjectsAndKeys:
                                  myArray, @"data",
                                  token ,@"auth_token",
                                  nil];
        
        
        NSData* jsonDataGreen = [NSJSONSerialization dataWithJSONObject:bulbData
                                                                options:NSJSONWritingPrettyPrinted
                                                                  error:&error];
        
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:@"https://huantengsmart.com/bulbs/request_update.json"]]; // Assumes you have created an NSURL * in "myURL"
        [request setHTTPMethod:@"PUT"];
        [request setHTTPBody:jsonDataGreen];
        [request setValue:@"application/json" forHTTPHeaderField:@"content-type"];
        
        NSError *theError = nil;
        NSURLResponse *theResponse =[[NSURLResponse alloc]init];
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&theResponse error:&theError];
        NSLog(@"response : %@", theResponse);
        NSLog(@"data : %@", data);
        
        NSString *string = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        NSLog(@"string: %@", string);
        //print out the data contents
        //    jsonSummary.text = [[NSString alloc] initWithData:jsonData
        //                                             encoding:NSUTF8StringEncoding];
        
    });
}

/*  backup --
 NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData //1
 options:kNilOptions
 error:&error];
 NSArray* latestLoans = [json objectForKey:@"loans"]; //2
 
 //    NSLog(@"loans: %@", latestLoans); //3
 
 // 1) Get the latest loan
 NSDictionary* loan = [latestLoans objectAtIndex:0];
 
 NSLog(@"loan numbers: %d", latestLoans.count); //3
 
 // 2) Get the funded amount and loan amount
 NSNumber* fundedAmount = [loan objectForKey:@"funded_amount"];
 NSNumber* loanAmount = [loan objectForKey:@"loan_amount"];
 float outstandingAmount = [loanAmount floatValue] - [fundedAmount floatValue];
 
 // 3) Set the label appropriately
 humanReadble.text = [NSString stringWithFormat:@"Latest loan: %@ from %@ needs another $%.2f to pursue their entrepreneural dream",
 [loan objectForKey:@"name"],
 [(NSDictionary*)[loan objectForKey:@"location"] objectForKey:@"country"],
 outstandingAmount
 ];
 
 //build an info object and convert to json
 NSDictionary* info = [NSDictionary dictionaryWithObjectsAndKeys:
 [loan objectForKey:@"name"], @"who",
 [(NSDictionary*)[loan objectForKey:@"location"] objectForKey:@"country"], @"where",
 [NSNumber numberWithFloat: outstandingAmount], @"what",
 nil];
 
 //convert object to data
 NSData* jsonData = [NSJSONSerialization dataWithJSONObject:info
 options:NSJSONWritingPrettyPrinted
 error:&error];
 
 //print out the data contents
 jsonSummary.text = [[NSString alloc] initWithData:jsonData
 encoding:NSUTF8StringEncoding];
 */

@end
