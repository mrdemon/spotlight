//
//  CircleUpdateView.m
//  KivaJSONDemo
//
//  Created by Damon Chu on 13/7/27.
//
//

#import "CircleUpdateView.h"

@implementation CircleUpdateView

@synthesize circleColor = _circleColor;
@synthesize clockwise = _clockwise;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        circleFillRate = 100;
        _circleColor = [UIColor blueColor];
        _clockwise = 0;
    }
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextClearRect(context, rect);
    CGContextSetLineWidth(context, 18.5);
    CGContextSetStrokeColorWithColor(context, _circleColor.CGColor);
//    NSLog(@"Draw degree = (%f, %f)", -M_PI_2, -M_PI_2+M_PI*2*circleFillRate/100);
    CGContextBeginPath(context);
    CGContextAddArc(context, rect.size.width/2, rect.size.height/2, 86.5, -M_PI_2, -M_PI_2+M_PI*2*circleFillRate/100, _clockwise);
    CGContextStrokePath(context);
}

- (void)updateFillRate:(int)rate{
    circleFillRate = rate;
    NSLog(@"fill rate: %d", rate);
    [self setNeedsDisplay];
}

@end
