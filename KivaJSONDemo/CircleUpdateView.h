//
//  CircleUpdateView.h
//  KivaJSONDemo
//
//  Created by Damon Chu on 13/7/27.
//
//

#import <UIKit/UIKit.h>

@interface CircleUpdateView : UIView{
    int circleFillRate;
    int radius;
}

- (void)updateFillRate:(int)rate;

@property(nonatomic, strong) UIColor* circleColor;
@property(nonatomic, assign) int clockwise;
@end
