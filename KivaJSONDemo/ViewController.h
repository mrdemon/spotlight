//
//  ViewController.h
//  KivaJSONDemo
//
//  Created by Ray Wenderlich on 10/3/11.
//  Copyright 2011 Razeware LLC. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreAudio/CoreAudioTypes.h>
#import <CoreMotion/CoreMotion.h>
#import "CircleUpdateView.h"
#import "AsyncUdpSocket.h" // for UDP socket

@interface ViewController : UIViewController {
    AVAudioRecorder* recorder;
    CMMotionManager* manager;
    NSTimer* levelTimer;
    double lowPassResults;
    int rollLevel;
    NSString* token;
    
    NSDateFormatter* dateFormater;
    NSDateFormatter* SecsFormater;
    CircleUpdateView* circleView;
    UIButton* actionBtn;
    NSTimer* pomodoroTimer;
    NSDate* startTime;
    UILabel* curTimeLabel;
    UILabel* curSecsLabel;
    UILabel* gLeafLabel;
    UILabel* bLeafLabel;
    NSTimeInterval workSecs;    // working minutes
    NSTimeInterval sRestSecs;   // small rest minutes
    NSTimeInterval lRestSecs;   // large rest minutes
    int gLeafCount;
    int bLeafCount;
    int lRestInterval;  // Interval for large rest
    int curSRestTimes;  // times for small rest
    int currentType;    // 0: nothing, 1: working,
                        // 2: small rest, 3: large rest
    
    AsyncUdpSocket *udpSocket;
}

@end
